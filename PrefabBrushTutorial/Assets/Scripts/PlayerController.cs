﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour 
{

	public float speed;
	public float jumpForce;

	private AudioSource sounds;
	private Animator anim;
	private Rigidbody2D rb2d;
	private SpriteRenderer sprite;

	private float axisHorizontal;
	private bool jump;
	private bool grounded;
	private bool walking;

	private void Awake() 
	{
		sounds = GetComponent<AudioSource>();
		anim = GetComponent<Animator>();
		rb2d = GetComponent<Rigidbody2D>();
		sprite = GetComponentInChildren<SpriteRenderer>();
	}

	void FixedUpdate() 
	{
		Move();
		Flip();

		if(jump && grounded)
		{
			Jump();
		}

		walking = rb2d.velocity.x != 0;
	}

	private void Update() 
	{
		axisHorizontal = Input.GetAxisRaw("Horizontal");

		jump = Input.GetButton("Jump");

		anim.SetBool("grounded", grounded);
		anim.SetBool("walking", walking);
	}

	private void Jump()
	{
		anim.SetTrigger("jump");

		jump = false;
		rb2d.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
	}

	private void Move()
	{
		Vector2 direction = new Vector2(axisHorizontal, 0);
		Vector2 velocity = rb2d.velocity;
		velocity.x = direction.x * speed;
		
		rb2d.velocity = velocity;
	}

	private void Flip()
	{
		if(axisHorizontal != 0)
		{
			sprite.flipX = axisHorizontal < 0;
		}
	}

	private void OnTriggerEnter2D(Collider2D other) 
	{
		if(other.CompareTag("Coin"))
		{
			sounds.Play();
			Destroy(other.gameObject);
		}
	}

	private void OnTriggerStay2D(Collider2D other) 
	{
		if(other.CompareTag("Ground"))
			grounded = true;
	}

	private void OnTriggerExit2D(Collider2D other) 
	{
		if(other.CompareTag("Ground"))
			grounded = false;
	}
}
