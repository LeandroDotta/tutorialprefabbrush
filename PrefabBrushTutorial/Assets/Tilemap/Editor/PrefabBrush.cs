﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(menuName = "Brush.../Prefab Brush", fileName = "New Prefab Brush")]
[CustomGridBrush(false, true, false, "Prefab Brush")]
public class PrefabBrush : GridBrushBase
{
	public GameObject prefab;

	public override void Paint(GridLayout grid, GameObject brushTarget, Vector3Int position)
	{
		if(brushTarget.layer == 31)
			return;

		//Instanciar o prefab
		GameObject instance = (GameObject)PrefabUtility.InstantiatePrefab(prefab);

		Undo.RegisterCreatedObjectUndo((Object)instance, "Paint Prefabs");

		//Colocar o objeto instanciado dentro da camada selecionada do tilemap
		instance.transform.SetParent(brushTarget.transform);
		instance.transform.position = grid.LocalToWorld(grid.CellToLocalInterpolated(position + new Vector3(0.5f, 0.5f, 0.5f)));
	}

	public override void Erase(GridLayout grid, GameObject brushTarget, Vector3Int position)
	{
		if(brushTarget.layer == 31)
			return;

		Vector2 min = grid.LocalToWorld(grid.CellToLocalInterpolated(position));
		Vector2 max = grid.LocalToWorld(grid.CellToLocalInterpolated(position + Vector3Int.one));

		Bounds cellBounds = new Bounds((min + max)*.5f, max - min);

		int childCount = brushTarget.transform.childCount;
		for(int i = 0; i < childCount; i++)
		{
			Transform child = brushTarget.transform.GetChild(i);
			if(cellBounds.Contains(child.position))
			{
				Undo.DestroyObjectImmediate(child.gameObject);
				return;
			}
		}
	}
}
